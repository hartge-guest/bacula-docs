#
#-------------------------------------------------------------------------

SHELL = /bin/sh

# Installation target directories & other installation stuff
prefix = /usr/local
exec_prefix = ${prefix}
binprefix =
manprefix =
sbindir = ${exec_prefix}/sbin
sysconfdir = ${prefix}/etc
scriptdir = @scriptdir@
mandir = ${datarootdir}/man/man1
manext = 1
datarootdir=${prefix}/share

# Tools & program stuff
MV = /bin/mv
RM = /bin/rm
RMF = /bin/rm -f
CP = /bin/cp
SED = @SED@
AWK = /usr/bin/gawk
ECHO = /bin/echo
CMP = @CMP@
INSTALL = /usr/bin/install -c
# add the -s to the following in PRODUCTION mode
INSTALL_PROGRAM = /usr/bin/install -c -m @SBINPERM@
INSTALL_DATA = /usr/bin/install -c -m 644
INSTALL_SCRIPT = /usr/bin/install -c -m @SBINPERM@
INSTALL_CONFIG = /usr/bin/install -c -m 640


# End of common section of the Makefile
#-------------------------------------------------------------------------

srcdir =	.

.PATH:		.
BACULASRC =	/home/kern/bee/bacula/bacula

basedir = ..
topdir = ..
thisdir = docs

#
# Distribution variables
#

de_dirs = manuals/de/console manuals/de/developers manuals/de/main \
  manuals/de/misc manuals/de/problems manuals/de/utility


en_dirs = manuals/en/console manuals/en/developers manuals/en/main \
  manuals/en/misc manuals/en/problems manuals/en/utility

es_dirs = manuals/es/console manuals/es/developers manuals/es/main \
  manuals/es/misc manuals/es/problems manuals/es/utility

fr_dirs = manuals/fr/console manuals/fr/developers manuals/fr/main \
  manuals/fr/misc manuals/fr/problems manuals/fr/utility

all_dirs = ${de_dirs} ${en_dirs} ${es_dirs} ${fr_dirs}

LANGUAGES= en fr de es

#-------------------------------------------------------------------------

#en: pdftex

all: pdftex web clean
#	@for I in ${en_dirs}; \
#	  do (cd $$I; echo "==>Entering directory `pwd`"; \
#	      $(MAKE) $@ || (echo ""; echo ""; echo "  ====== Error in `pwd` ======"; \
#			    echo ""; echo ""; exit 1;)); \
#	done
#	@echo "All manuals built ..."


external-references:
	@echo " "
	@echo -n "Building external references file..."
	@find ${en_dirs} -mindepth 0 -maxdepth 1 -name "*tex" -exec grep -q '\label' {} \; -print| awk -F/ '{ print "\\externaldocument[" $$3 "-]{../"$$3"/"$$4"}"}'|sed -e 's/.tex//g' > `pwd`/latex/external-references.tex
	@for I in ${en_dirs}; \
	  do (cd $$I; cp -f ../../version.tex .; cp -f ../../bacula.sty .;); \
	done
	@for I in ${en_dirs}; \
	  do (cd $$I; echo "==>Entering directory `pwd`"; \
	      $(MAKE) $@ || (echo ""; echo ""; echo "  ====== Error in `pwd` ======"; \
			    echo ""; echo ""; exit 1;)); \
	done
	@echo "External references for all manuals built."

pdftex: external-references
	mktexlsr ~/texmf
	@for I in ${en_dirs}; \
	  do (cd $$I; cp -f ../../version.tex .; cp -f ../../bacula.sty .; ); \
	done
	@for I in ${en_dirs}; \
	  do (cd $$I; echo "==>Entering directory `pwd`"; \
	      $(MAKE) $@ || (echo ""; echo ""; echo "  ====== Error in `pwd` ======"; \
			    echo ""; echo ""; exit 1;)); \
	done
	@echo "All manuals built ..."

web:
	@for I in ${en_dirs}; \
	  do (cd $$I; cp -f ../../version.tex .; cp -f ../../bacula.sty .;); \
	done
	@for I in ${en_dirs}; \
	  do (cd $$I; echo "==>Entering directory `pwd`"; \
	      $(MAKE) $@ || (echo ""; echo ""; echo "  ====== Error in `pwd` ======"; \
			    echo ""; echo ""; exit 1;)); \
	done
	@(cd tools ; ./htmls.sh)
	@cp ./images/png/*.png manuals/en/pdf-and-html/images/
	@cp ./images/*.png manuals/en/pdf-and-html/images/
	@echo "All manuals built ..."


fr:	french

french:
	@for I in ${fr_dirs}; do (cd $$I; cp -f ../../version.tex .; cp -f ../../bacula.sty .;); done
	@for I in ${fr_dirs}; \
	  do (cd $$I; echo "==>Entering directory `pwd`"; \
	      $(MAKE) all || (echo ""; echo ""; echo "	====== Error in `pwd` ======"; \
			    echo ""; echo ""; exit 1;)); \
	done

de:	german

german:
	@for I in ${de_dirs}; do (cd $$I; cp -f ../.. .; cp -f ../../bacula.sty .;); done
	@for I in ${de_dirs}; \
	  do (cd $$I; echo "==>Entering directory `pwd`"; \
	      $(MAKE) all || (echo ""; echo ""; echo "	====== Error in `pwd` ======"; \
			    echo ""; echo ""; exit 1;)); \
	done

es:	spanish

spanish:
	@for I in ${es_dirs}; do (cd $$I; cp -f ../../version.tex .; cp -f ../../bacula.sty .;); done
	@for I in ${es_dirs}; \
	  do (cd $$I; echo "==>Entering directory `pwd`"; \
	      $(MAKE) all || (echo ""; echo ""; echo "	====== Error in `pwd` ======"; \
			    echo ""; echo ""; exit 1;)); \
	done


$(basedir)/$(VERNAME).lsm: LSM.in $(srcdir)/../autoconf/Make.common.in $(srcdir)/../src/version.h
	$(SED) $(SEDREPLACE) < $(srcdir)/LSM.in > $@

clean:
	$(RMF) *~ 1 2 3 bacula-doc*.tar.gz
	find . -type l -exec rm {} \;
	@for I in ${all_dirs}; \
	  do (cd $$I; echo "==>Entering directory `pwd`"; ${MAKE} $@ || exit 1); done
	(cd manuals/en/pdf-and-html; $(RMF) -r *.aux *.out *.idx *.toc *.cdx *.ddx *.fdx *.log *.sdx)

mini-clean:
	$(RMF) *~ 1 2 3 bacula-doc*.tar.gz
	find . -type l -exec rm {} \;
	@for I in ${all_dirs}; \
	  do (cd $$I; echo "==>Entering directory `pwd`"; ${MAKE} $@ || exit 1); done

realclean: clean

distclean: clean
	$(RMF) -r CVS html-manual/CVS home-page/CVS techlogs/CVS
	$(RMF) -rf autom4te.cache bacula-doc-* config.log config.out
	$(RMF) -f config.status kernsconfig
	@for I in ${all_dirs}; \
	  do (cd $$I; echo "==>Entering directory `pwd`"; ${MAKE} $@ || exit 1); done
	@echo "Removing the HTML directories..."
	@for L in $(LANGUAGES) ; \
	do rm -rf manuals/$$L/pdf-and-html ; done
	@echo "HTML directories removed."
	@echo "Cleaning the latex directory..."
	@rm -f latex/external-references
	@echo "latex directory cleaned up"


depend:

install:
#	$(INSTALL_DATA) bacula.1 $(DESTDIR)$(mandir)/$(manprefix)bacula.$(manext)

uninstall:
#	-cd $(mandir); $(RMF) $(manprefix)bacula.$(manext)
