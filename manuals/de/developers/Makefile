#
#
#  Makefile for LaTeX  
#
# To build everything do
#    make tex
#    make web
#    make html
#    make dvipdf
#
# or simply
#
#    make
#

IMAGES=../../../images

DOC=developers

first_rule: all

all: tex web pdf mini-clean

.SUFFIXES:     .tex .html
.PHONY:
.DONTCARE:


tex:
	@../../update_version
	@cp -fp ${IMAGES}/hires/*.eps .
	touch ${DOC}.idx ${DOC}i-general.tex
	-latex -interaction=batchmode ${DOC}.tex
	makeindex ${DOC}.idx >/dev/null 2>/dev/null
	-latex -interaction=batchmode ${DOC}.tex

pdf:
	@echo "Making ${DOC} pdf"
	@cp -fp ${IMAGES}/hires/*.eps .
	dvipdf ${DOC}.dvi ${DOC}.pdf
	@rm -f *.eps *.old

dvipdf:
	@echo "Making ${DOC} pdfm"
	@cp -fp ${IMAGES}/hires/*.eps .
	dvipdfm -p a4 ${DOC}.dvi >tex.out 2>&1

html:
	@echo "Making ${DOC} html"
	@cp -fp ${IMAGES}/*.eps .
	@rm -f next.eps next.png prev.eps prev.png up.eps up.png
	@touch ${DOC}.html
	@(if [ -f imagename_translations ] ; then \
	    ./translate_images.pl --from_meaningful_names ${DOC}.html; \
	 fi)
	latex2html -white -no_subdir -split 0 -toc_stars -white -notransparent \
		${DOC} >tex.out 2>&1
	@(if [ -f imagename_translations ] ; then \
	    ./translate_images.pl --from_meaningful_names ${DOC}.html; \
	 fi)
	(cd ${DOC}; for i in *.png ; do cp -fp ../${IMAGES}/$${i} . 2>/dev/null; done)
	@rm -f *.eps *.gif *.jpg *.old

web:
	@echo "Making ${DOC} web"
	@rm -rf ${DOC}
	@mkdir -p ${DOC}
	@rm -f ${DOC}/*
	@cp -fp ${IMAGES}/*.eps .
	@rm -f next.eps next.png prev.eps prev.png up.eps up.png
	@(if [ -f ${DOC}/imagename_translations ] ; then \
	   ./translate_images.pl --to_meaningful_names ${DOC}/Developer*Guide.html; \
	 fi)
	@rm -rf ${DOC}/*.html
	latex2html -split 4 -local_icons -t "Developer's Guide" -long_titles 4 \
		-toc_stars -contents_in_nav -init_file latex2html-init.pl \
		-no_antialias -no_antialias_text \
		-white -notransparent ${DOC} >tex.out 2>&1
	@(if [ -f imagename_translations ] ; then \
	    ./translate_images.pl --from_meaningful_names ${DOC}.html; \
	 fi)
	@cp -f ${DOC}/Bacula_Developer_Notes.html ${DOC}/index.html
	@rm -f *.eps *.gif *.jpg ${DOC}/*.eps  *.old 
	@rm -f ${DOC}/idle.png
	@rm -f ${DOC}/win32-*.png ${DOC}/wx-console*.png ${DOC}/xp-*.png
	@rm -f ${DOC}/*.pl ${DOC}/*.log ${DOC}/*.aux ${DOC}/*.idx
	@rm -f ${DOC}/*.out WARNINGS

texcheck:
	./check_tex.pl ${DOC}.tex

main_configs:
	pic2graph -density 100 <main_configs.pic >main_configs.png

mini-clean:
	@rm -f 1 2 3 *.tex~
	@rm -f *.gif *.jpg *.eps
	@rm -f *.aux *.cp *.fn *.ky *.log *.pg
	@rm -f *.backup *.ilg *.lof *.lot
	@rm -f *.cdx *.cnd *.ddx *.ddn *.fdx *.fnd *.ind *.sdx *.snd
	@rm -f *.dnd *.old *.out 
	@rm -f ${DOC}/*.gif ${DOC}/*.jpg ${DOC}/*.eps
	@rm -f ${DOC}/*.aux ${DOC}/*.cp ${DOC}/*.fn ${DOC}/*.ky ${DOC}/*.log ${DOC}/*.pg
	@rm -f ${DOC}/*.backup ${DOC}/*.ilg ${DOC}/*.lof ${DOC}/*.lot
	@rm -f ${DOC}/*.cdx ${DOC}/*.cnd ${DOC}/*.ddx ${DOC}/*.ddn ${DOC}/*.fdx ${DOC}/*.fnd ${DOC}/*.ind ${DOC}/*.sdx ${DOC}/*.snd
	@rm -f ${DOC}/*.dnd ${DOC}/*.old ${DOC}/*.out
	@rm -f ${DOC}/WARNINGS

clean:
	@rm -f 1 2 3
	@rm -f *.png *.gif *.jpg *.eps
	@rm -f *.pdf *.aux *.cp *.fn *.ky *.log *.pg
	@rm -f *.html *.backup *.pdf *.ps *.dvi *.ilg *.lof *.lot
	@rm -f *.cdx *.cnd *.ddx *.ddn *.fdx *.fnd *.ind *.sdx *.snd
	@rm -f *.dnd imagename_translations
	@rm -f *.old WARNINGS *.out *.toc *.idx
	@rm -f images.pl labels.pl internals.pl
	@rm -rf ${DOC}
	@rm -f images.tex ${DOC}i.tex
	@rm -f ${DOC}i-*.tex


distclean:  clean
	@rm -f ${DOC}.html ${DOC}.pdf
