\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Bacula Console}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Console Configuration}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Running the Console Program}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Stopping the Console Program}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Alphabetic List of Console Keywords}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Alphabetic List of Console Commands}{4}{section.1.5}
\contentsline {section}{\numberline {1.6}Special dot Commands}{15}{section.1.6}
\contentsline {section}{\numberline {1.7}Special At (@) Commands}{16}{section.1.7}
\contentsline {section}{\numberline {1.8}Running the Console from a Shell Script}{16}{section.1.8}
\contentsline {section}{\numberline {1.9}Adding Volumes to a Pool}{17}{section.1.9}
\contentsline {chapter}{\numberline {2}Baculum API and Web GUI Tools}{19}{chapter.2}
\contentsline {section}{\numberline {2.1}Base Features}{19}{section.2.1}
\contentsline {section}{\numberline {2.2}General Requirements}{19}{section.2.2}
\contentsline {section}{\numberline {2.3}Installation Baculum API from rpm binary packages}{20}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Add the Baculum rpm repository}{20}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Installation for the Apache}{21}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Installation for Lighttpd}{21}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Access to bconsole via sudo for Apache and Lighttpd}{21}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}Installation Baculum API from deb binary packages}{21}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Add the Baculum deb repository}{22}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Installation for Apache}{22}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Installation for Lighttpd}{22}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Access to bconsole via sudo for Apache and Lighttpd}{23}{subsection.2.4.4}
\contentsline {section}{\numberline {2.5}Debugging your First Baculum API Login}{23}{section.2.5}
\contentsline {section}{\numberline {2.6}Installation Baculum Web from rpm binary packages}{23}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Installation for the Apache}{23}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Installation for Lighttpd}{23}{subsection.2.6.2}
\contentsline {section}{\numberline {2.7}Installation Baculum Web from deb binary packages}{24}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Installation for Apache}{24}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}Installation for Lighttpd}{24}{subsection.2.7.2}
\contentsline {section}{\numberline {2.8}Running Baculum API and Web for the First Time}{24}{section.2.8}
\contentsline {subsection}{\numberline {2.8.1}Running Baculum API}{24}{subsection.2.8.1}
\contentsline {subsection}{\numberline {2.8.2}Running Baculum Web}{24}{subsection.2.8.2}
\contentsline {section}{\numberline {2.9}Installation wizards}{24}{section.2.9}
\contentsline {section}{\numberline {2.10}Current Baculum API documentation}{28}{section.2.10}
\contentsline {section}{\numberline {2.11}Baculum API endpoints (obsolete)}{28}{section.2.11}
\contentsline {subsection}{\numberline {2.11.1}Clients}{28}{subsection.2.11.1}
\contentsline {subsection}{\numberline {2.11.2}Storages}{28}{subsection.2.11.2}
\contentsline {subsection}{\numberline {2.11.3}Volumes}{29}{subsection.2.11.3}
\contentsline {subsection}{\numberline {2.11.4}Pools}{29}{subsection.2.11.4}
\contentsline {subsection}{\numberline {2.11.5}Jobs}{29}{subsection.2.11.5}
\contentsline {subsection}{\numberline {2.11.6}Bvfs}{32}{subsection.2.11.6}
\contentsline {subsection}{\numberline {2.11.7}Config}{34}{subsection.2.11.7}
\contentsline {section}{\numberline {2.12}Installation from the Source Tar File}{35}{section.2.12}
\contentsline {section}{\numberline {2.13}OAuth2 authorization}{36}{section.2.13}
\contentsline {subsection}{\numberline {2.13.1}Before running OAuth2}{36}{subsection.2.13.1}
\contentsline {section}{\numberline {2.14}Multi-user interface}{37}{section.2.14}
\contentsline {section}{\numberline {2.15}Screenshots}{42}{section.2.15}
\contentsline {chapter}{\numberline {3}Bacula Copyright, Trademark, and Licenses}{47}{chapter.3}
\contentsline {section}{\numberline {3.1}CC-BY-SA}{47}{section.3.1}
\contentsline {section}{\numberline {3.2}GPL}{47}{section.3.2}
\contentsline {section}{\numberline {3.3}LGPL}{47}{section.3.3}
\contentsline {section}{\numberline {3.4}Public Domain}{47}{section.3.4}
\contentsline {section}{\numberline {3.5}Trademark}{47}{section.3.5}
\contentsline {section}{\numberline {3.6}Fiduciary License Agreement}{47}{section.3.6}
\contentsline {section}{\numberline {3.7}Disclaimer}{48}{section.3.7}
\contentsline {section}{\numberline {3.8}Authors}{48}{section.3.8}
