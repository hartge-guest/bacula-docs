%%
%%

\chapter{Installing and Configuring PostgreSQL}
\label{PostgreSqlChapter}
\index[general]{PostgreSQL!Installing and Configuring }
\index[general]{Installing and Configuring PostgreSQL }
\index[general]{Upgrading}

If you have installed PostgreSQL from a package (.deb, .rpm, ...) or 
already installed it from source, please skip to the next section.

If you are building PostgreSQL from source, please be sure to add
the {\bf \verb:--:enable-thread-safety} option when doing the ./configure
for PostgreSQL.  

\section{Installing PostgreSQL}
\index[general]{PostgreSQL!Installing }

If you use the {\bf ./configure \verb:--:with-postgresql=PostgreSQL-Directory}
statement for configuring {\bf Bacula}, you will need PostgreSQL version 7.4
or later installed. NOTE! PostgreSQL versions earlier than 7.4 do not work
with Bacula. If PostgreSQL is installed in the standard system location, you
need only enter {\bf \verb:--:with-postgresql} since the configure program will
search all the standard locations. If you install PostgreSQL in your home
directory or some other non-standard directory, you will need to provide the
full path with the {\bf \verb:--:with-postgresql} option. 

Installing and configuring PostgreSQL is not difficult but can be confusing
the first time. If you prefer, you may want to use a package provided by your
chosen operating system. Binary packages are available on most PostgreSQL
mirrors. 

If you prefer to install from source, we recommend following the instructions
found in the 
\elink{PostgreSQL Documentation}{http://www.postgresql.org/docs/}. 

If you are using FreeBSD, 
\elink{this FreeBSD Diary article}{http://www.freebsddiary.org/postgresql.php}
will be useful. Even if you are not using FreeBSD, the article will contain
useful configuration and setup information. 

If you configure the Batch Insert code in Bacula (attribute inserts are
10 times faster), you {\bf must} be using a PostgreSQL that was built with
the {\bf \verb:--:enable-thread-safety} option, otherwise you will get
data corruption. Most major Linux distros have thread safety turned on, but
it is better to check.  One way is to see if the PostgreSQL library that
Bacula will be linked against references pthreads.  This can be done
with a command such as:

\footnotesize
\begin{verbatim}
  nm /usr/lib/libpq.a | grep pthread_mutex_lock
\end{verbatim}
\normalsize

The above command should print a line that looks like:

\footnotesize
\begin{verbatim}
         U pthread_mutex_lock
\end{verbatim}
\normalsize

if does, then everything is OK. If it prints nothing, do not enable batch
inserts when building Bacula.

After installing PostgreSQL, you should return to completing the installation
of {\bf Bacula}. Later, after Bacula is installed, come back to this chapter
to complete the installation. Please note, the installation files used in the
second phase of the PostgreSQL installation are created during the Bacula
Installation. You must still come back to complete the second phase of the 
PostgreSQL installation even if you installed binaries (e.g. rpm, deb,
...).


\label{PostgreSQL_configure}
\section{Configuring PostgreSQL}
\index[general]{PostgreSQL!Configuring PostgreSQL -- }

At this point, you should have built and installed PostgreSQL, or already have
a running PostgreSQL, and you should have configured, built and installed {\bf
Bacula}. If not, please complete these items before proceeding. 

Please note that the {\bf ./configure} used to build {\bf Bacula} will need to
include {\bf \verb:--:with-postgresql=PostgreSQL-directory}, where {\bf
PostgreSQL-directory} is the directory name that you specified on the
./configure command for configuring PostgreSQL (if you didn't specify a
directory or PostgreSQL is installed in a default location, you do not need to
specify the directory). This is needed so that Bacula can find the necessary
include headers and library files for interfacing to PostgreSQL. 

An important thing to note here is that {\bf Bacula} makes two connections
to the PostgreSQL server for each backup job that is currently running.  If
you are intending to run a large number of concurrent jobs, check the value
of {\bf max\_connections} in your PostgreSQL configuration file to ensure
that it is larger than the setting {\bf Maximum Concurrent Jobs}
in your director configuration.  {\bf Setting this too low will result in
some backup jobs failing to run correctly!}

{\bf Bacula} will install scripts for manipulating the database (create,
delete, make tables etc) into the main installation directory. These files
will be of the form *\_bacula\_* (e.g. create\_bacula\_database). These files
are also available in the \lt{}bacula-src\gt{}/src/cats directory after
running ./configure. If you inspect create\_bacula\_database, you will see
that it calls create\_postgresql\_database. The *\_bacula\_* files are
provided for convenience. It doesn't matter what database you have chosen;
create\_bacula\_database will always create your database. 

Now you will create the Bacula PostgreSQL database and the tables that Bacula
uses. These instructions assume that you already have PostgreSQL running. You
will need to perform these steps as a user that is able to create new
databases. This can be the PostgreSQL user (on most systems, this is the pgsql
user). 

\begin{enumerate}
\item cd \lt{}install-directory\gt{}

   This directory contains the Bacula catalog interface routines.  

\item Create the database owner ({\bf bacula})
   On many systems, the PostreSQL master 
   owner is {\bf pgsql} and on others such as Red Hat and Fedora it is {\bf
   postgres}.  You can find out which it is by examining your /etc/passwd
   file.  To create a new user under either your name or with say the name
   {\bf bacula}, you can do the following:

\begin{verbatim}
   su
   (enter root password)
   su pgsql (or postgres)
   createuser bacula
   Shall the new user be allowed to create databases? (y/n) y
   Shall the new user be allowed to create more new users? (y/n) (choose
         what you want)
   exit
\end{verbatim}

   In newer versions of PostgreSQL, it will not automatically ask
   if the user {\bf bacula} should be able to create databases or
   create new users. To do so you may need to use the {\bf -s}
   option on the {\bf createuser} command.  Example:

\begin{verbatim}
   createuser -s bacula
\end{verbatim}

   Normally the {\bf bacula} user must be able to create new databases, if
   you use the script in the next item, or you will have to create one for
   it, but it does not need to create new users.


\item ./create\_bacula\_database

   This script creates the PostgreSQL {\bf bacula} database.  
   Before running this command, you should carefully think about
   what encoding sequence you want for the text fields (paths, files, ...).
   We strongly recommend that you use the default value of SQL\_ASCII
   that is in the create\_bacula\_database script.  Please be warned
   that if you change this value, your backups may fail.  After running
   the script, you can check with the command:

\begin{verbatim}
   psql -l
\end{verbatim}

   and the column marked {\bf Encoding} should be {\bf SQL\_ASCII} for
   all your Bacula databases (normally {\bf bacula}).

\item ./make\_bacula\_tables

   This script creates the PostgreSQL tables used by {\bf Bacula}.  
\item ./grant\_bacula\_privileges

   This script creates the database user {\bf bacula}  with restricted access
rights. You may  want to modify it to suit your situation. Please note that 
this database is not password protected.  

\end{enumerate}

Each of the three scripts (create\_bacula\_database, make\_bacula\_tables, and
grant\_bacula\_privileges) allows the addition of a command line argument.
This can be useful for specifying the user name. For example, you might need
to add {\bf -h hostname} to the command line to specify a remote database
server. 

To take a closer look at the access privileges that you have setup with the
above, you can do: 

\footnotesize
\begin{verbatim}
PostgreSQL-directory/bin/psql --command \\dp bacula
\end{verbatim}
\normalsize

For people who are not Postgresql experts, it is sometimes difficult
to get the authorization working correctly with Bacula.
One simple, but not recommended way, to authorize Bacula is
to modify your {\bf pg\_hba.conf} file (in /var/lib/pgsql/data 
or in /var/lib/postgresql/8.x or in /etc/postgres/8.x/main on
other distributions) from:

\footnotesize
\begin{verbatim}
  local   all    all        ident  sameuser
to
  local   all    all        trust
\end{verbatim}
\normalsize

This is a quick way to solve the problem, but it is not always a good thing
to do from a security standpoint.  However, it allows one to run
my regression scripts without having a password.

A more secure way to perform database authentication is with md5
password hashes.  Begin by editing the {\bf pg\_hba.conf} file, and
above the existing {\bf local} and {\bf host} lines, add the line:

\footnotesize
\begin{verbatim}
  local bacula bacula md5
\end{verbatim}
\normalsize

then restart the Postgres database server (frequently, this can be done
using "/etc/init.d/postgresql restart" or "service postgresql restart") to
put this new authentication rule into effect.

\smallskip
More detailed information on the {\bf pg\_hba.conf} file can be found at
\elink{PostgreSQL pg\_hba.conf Documentation}
{https://www.postgresql.org/docs/10/auth-pg-hba-conf.html}

\smallskip

Next, become the Postgres administrator, postgres, either by logging
on as the postgres user, or by using su to become root and then using
{\bf su - postgres} or {\bf su - pgsql} to become postgres.  
Add a password to the {\bf bacula} database for the {\bf bacula} user using:

\footnotesize
\begin{verbatim}
  \$ psql bacula
  bacula=# alter user bacula with password 'secret';
  ALTER USER
  bacula=# \\q
\end{verbatim}
\normalsize

You'll have to add this password to two locations in the
bacula-dir.conf file: once to the Catalog resource and once to the
RunBeforeJob entry in the BackupCatalog Job resource.  With the
password in place, these two lines should look something like:

\footnotesize
\begin{verbatim}
  dbname = bacula; user = bacula; password = "secret"
    ... and ...
  # WARNING!!! Passing the password via the command line is insecure.
  # see comments in make_catalog_backup for details.
  RunBeforeJob = "/etc/make_catalog_backup bacula bacula secret"
\end{verbatim}
\normalsize

Naturally, you should choose your own significantly more random
password, and ensure that the bacula-dir.conf file containing this
password is readable only by the root.

Even with the files containing the database password properly
restricted, there is still a security problem with this approach: on
some platforms, the environment variable that is used to supply the
password to Postgres is available to all users of the
local system.  To eliminate this problem, the Postgres team have
deprecated the use of the environment variable password-passing
mechanism and recommend the use of a .pgpass file instead.  To use
this mechanism, create a file named .pgpass containing the single
line:

\footnotesize
\begin{verbatim}
  localhost:5432:bacula:bacula:secret
\end{verbatim}
\normalsize

This file should be copied into the home directory of all accounts
that will need to gain access to the database: typically, root,
bacula, and any users who will make use of any of the console
programs.  The files must then have the owner and group set to match
the user (so root:root for the copy in root, and so on), and the mode
set to 600, limiting access to the owner of the file.

\section{Re-initializing the Catalog Database}
\index[general]{Database!Re-initializing the Catalog }
\index[general]{Re-initializing the Catalog Database }

After you have done some initial testing with {\bf Bacula}, you will probably
want to re-initialize the catalog database and throw away all the test Jobs
that you ran. To do so, you can do the following: 

\footnotesize
\begin{verbatim}
  cd <install-directory>
  ./drop_bacula_tables
  ./make_bacula_tables
  ./grant_bacula_privileges
\end{verbatim}
\normalsize

Please note that all information in the database will be lost and you will be
starting from scratch. If you have written on any Volumes, you must write an
end of file mark on the volume so that Bacula can reuse it. Do so with: 

\footnotesize
\begin{verbatim}
   (stop Bacula or unmount the drive)
   mt -f /dev/nst0 rewind
   mt -f /dev/nst0 weof
\end{verbatim}
\normalsize

Where you should replace {\bf /dev/nst0} with the appropriate tape drive
device name for your machine. 

\section{Installing PostgreSQL from RPMs}
\index[general]{PostgreSQL!Installing from RPMs}
\index[general]{Installing PostgreSQL from RPMs}
If you are installing PostgreSQL from RPMs, you will need to install
both the PostgreSQL binaries and the client libraries.  The client
libraries are usually found in a {\bf devel} or {\bf dev} package, so you must
install the following for rpms:

\footnotesize
\begin{verbatim}
  postgresql
  postgresql-devel
  postgresql-server
  postgresql-libs
\end{verbatim}
\normalsize


and the following for debs:

\footnotesize
\begin{verbatim}
  postgresql
  postgresql-common
  postgresql-client
  postgresql-client-common
  libpq5
  libpq-dev
\end{verbatim}
\normalsize


These will be similar with most other package managers too.  After
installing from rpms, you will still need to run the scripts that set up
the database and create the tables as described above.

\section{Converting from MySQL to PostgreSQL}
\index[general]{PostgreSQL!Converting from MySQL to }
\index[general]{Converting from MySQL to PostgreSQL }

There are various scripts available that allow you to convert from MySQL 
to PostgreSQL database format.  The first step is to backup your existing
MySQL database, then shutdown Bacula.

\smallskip
One such conversion program from MySQL to PostgreSQL can be found at:
\elink{MySQL to PostgreSQL Conversion Scripts}
{https://github.com/wanderleihuttel/bacula-utils/tree/master/convert_mysql_to_postgresql}

\section{Upgrading PostgreSQL}
\index[general]{Upgrading PostgreSQL }
\index[general]{Upgrading!PostgreSQL }
\index[general]{Upgrading}
If you upgrade PostgreSQL, on older PostgreSQLs, you needed to 
rebuild Bacula for the specific version of PostgreSQL.  However,
on more recent versions, this is no longer necessary. 
If you experience problems after upgrading PostgreSQL, you might
try rebuilding Bacula.

\section{Tuning PostgreSQL}
\index[general]{Tuning} 

If you despool attributes for many jobs at the same time, you can tune the
sequence object for the \texttt{FileId} field.
\begin{verbatim}
psql -Ubacula bacula

ALTER SEQUENCE file_fileid_seq CACHE 1000;
\end{verbatim}

\section{Credits}
\index[general]{Credits }
Many thanks to Dan Langille for writing the PostgreSQL driver. This will
surely become the most popular database that Bacula supports. 
